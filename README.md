# Appli

Lancer l'application en utilisant la commande :
```python
python3 main.py
```

## Dépendance 

```python
pip3 install -r requirements.txt
```

# Portabilité
- Un code n'est valide que s'il a été exécuté sur au moins 3 environnements différents.
- Tout le code doit être versionné en permanence sur git
- Pour transférer le code d'un environnement à l'autre, on passera par git (git push sur un des environnements et git pull sur les autres). Tout transfert de code entre les plateformes en utilisant un autre outil (mail, copier / coller, recopie manuelle ...) doit être proscrit
- Un code n'est valide que s'il est lançable depuis le terminal (donc sans utiliser les boutons de l'IDE).
- Un code n'est valide que s'il est lançable directement après un git clone, éventuellement avec une succession de commandes.
- Un code n'est valide que si la succession de commandes nécessaires au lancement est documentée dans un fichier README.md à la racine du projet.
